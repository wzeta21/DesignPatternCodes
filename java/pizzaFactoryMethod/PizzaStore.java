
/**
 * Client of The factory
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PizzaStore
{
    PizzaFactory factory;
    public PizzaStore(PizzaFactory factory)
    {
        this.factory = factory;
    }

    public IPizza orderPizza(String type) {
        
        IPizza pizza = factory.factoryMethod(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
