
/**
 * Producto of the factory
 * Abstract or Interface
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public interface IPizza
{
     public void prepare();
     public void bake();
     public void cut();
     public void box();
}
