
/**
 * Write a description of class CheesePizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class CheesePizza implements IPizza
{

    /**
     * Constructor for objects of class CheesePizza
     */
    public CheesePizza()
    {
        System.out.println("I am cheese pizza!");
    }

    public void prepare(){}

    public void bake(){}

    public void cut(){}

    public void box(){}
}
