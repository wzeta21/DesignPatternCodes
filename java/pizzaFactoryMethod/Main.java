
/**
 * Write a description of class Main here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Main
{
    public static void main(String arg[]) {
         SimplePizzaFactory pizzaFactoy = new SimplePizzaFactory();
         PizzaStore pizzaStore = new PizzaStore(pizzaFactoy);
         IPizza cheesePizza = pizzaStore.orderPizza("cheese");
         // IPizza cheesePizza = pizzaFactoy.factoryMethod("cheese");
         
         IPizza clamPizza = pizzaStore.orderPizza("clam"); 
         //IPizza clamPizza = pizzaFactoy.factoryMethod("clam");
    }
}
