
/**
 * Write a description of class SimplePizzaFactory here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SimplePizzaFactory extends PizzaFactory
{
    public SimplePizzaFactory()
    {

    }

    @Override
    public IPizza factoryMethod(String type) {
        IPizza pizza = null;
        if (type.equals("cheese")) 
        {
            pizza = new CheesePizza();
        } else if (type.equals("pepperoni")) {
            pizza = new PepperoniPizza();
        } else if (type.equals("clam")) {
            pizza = new ClamPizza();
        } else if (type.equals("veggie")) {
            pizza = new VeggiePizza();
        }
        return pizza;
    }
    ///Factory Method
    /*
    public static IPizza createPizza(String type) {
        IPizza pizza = null;
        if (type.equals("cheese")) 
        {
            pizza = new CheesePizza();
        } else if (type.equals("pepperoni")) {
            pizza = new PepperoniPizza();
        } else if (type.equals("clam")) {
            pizza = new ClamPizza();
        } else if (type.equals("veggie")) {
            pizza = new VeggiePizza();
        }
        return pizza;
    }
    */
}
