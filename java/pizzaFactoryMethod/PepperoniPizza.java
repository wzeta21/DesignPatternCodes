
/**
 * Write a description of class PepperoniPizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PepperoniPizza implements IPizza
{
    
    public PepperoniPizza()
    {
        System.out.println("I am pepperoni pizza!");
    }

    public void prepare(){}

    public void bake(){}

    public void cut(){}

    public void box(){}
}
