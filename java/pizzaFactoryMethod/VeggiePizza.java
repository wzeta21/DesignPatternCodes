
/**
 * Write a description of class VeggiePizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class VeggiePizza implements IPizza
{

    public VeggiePizza()
    {
        System.out.println("I am veggie pizza!");
    }

    public void prepare(){}

    public void bake(){}

    public void cut(){}

    public void box(){}
}
