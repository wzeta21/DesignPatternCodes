
/**
 * Write a description of class Main here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Main
{
    public static void main(String[] args)
    {
        CreatorAbstract creator = new Creator();
        
        IFile audio = creator.create( Creator.AUDIO );
        audio.play();
        
        IFile video = creator.create( Creator.VIDEO );
        video.play();
    }
}
