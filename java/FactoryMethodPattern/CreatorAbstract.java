/**
 * Abstract class CreadorAbstracto - write a description of the class here
 * 
 * @author: 
 * Date: 
 */
public abstract class CreatorAbstract
{
    public static final int AUDIO = 1;
    public static final int VIDEO = 2;
    //factory method
    public abstract IFile create(int tipo);
}
