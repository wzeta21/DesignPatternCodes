public class Creator extends CreatorAbstract
{
    public void Creator() {
    }
   
    @Override
     public IFile create(int tipo)
    {
        IFile objeto;
        switch( tipo )
        {
            case AUDIO:
                objeto = new AudioFile();
                break;
            case VIDEO:
                objeto = new VideoFile();
                break;
            default:
                objeto = null;
        }
        return objeto;
    }
}