
/**
 * Write a description of class NYPepperoniPizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NYPepperoniPizza extends Pizza
{
    public NYPepperoniPizza() {
        name = "NY Style Pepperoni Pizza";
        dough = "Thin Crust Dough";
        sauce = "Pepperoni Sauce";
        toppings.add("Grated Pepperoni");
    }
}
