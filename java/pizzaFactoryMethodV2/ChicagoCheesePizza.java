
/**
 * Write a description of class ChicagoCheesePizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ChicagoCheesePizza extends Pizza
{
    public ChicagoCheesePizza() {
        name = "Chicago Style Deep Dish Cheese Pizza";
        dough = "Extra Thick Crust Dough";
        sauce = "Plum Tomato Sauce";
        toppings.add("Shredded Mozzarella Cheese");
    }
    @Override
    public void cut() {
        System.out.println("Cutting the pizza into square slices");
    }
}
