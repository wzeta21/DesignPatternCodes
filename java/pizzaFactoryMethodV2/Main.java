
/**
 * Write a description of class Main here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Main
{
    public static void main(String arg[]) {
        // SimplePizzaFactory pizzaFactoy = new SimplePizzaFactory();

        // Pizza cheesePizza = pizzaFactoy.factoryMethod("cheese");

        // Pizza clamPizza = pizzaFactoy.factoryMethod("clam"); 

        /////
        PizzaStore nyStore = new NYPizzaStore();       
        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza.getName() + "\n");
        
        PizzaStore chicagoStore = new ChicagoPizzaStore();
        pizza = chicagoStore.orderPizza("cheese");
        System.out.println("Joel ordered a " + pizza.getName() + "\n");
    }
}
