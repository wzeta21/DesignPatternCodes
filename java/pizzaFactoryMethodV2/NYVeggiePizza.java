
/**
 * Write a description of class NYVeggiePizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NYVeggiePizza extends Pizza
{
   public NYVeggiePizza() {
        name = "NY Style Veggied Pizza";
        dough = "Thin Crust Dough";
        sauce = "Veggied Sauce";
        toppings.add("Grated Veggied");
    }
}
