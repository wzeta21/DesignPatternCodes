
/**
 * Write a description of class ChicagoPizzaStore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ChicagoPizzaStore extends PizzaStore
{
    @Override
    public Pizza createPizza(String type)
    {
        Pizza pizza = null; 
        if (type.equals("cheese")) {
            pizza = new ChicagoCheesePizza();
        } else if (type.equals("pepperoni")){
            pizza = new ChicagoPepperoniPizza();
        } else if (type.equals("clam")){
            pizza = new ChicagoClamPizza();
        } else if (type.equals("veggie")){
            pizza = new ChicagoVeggiePizza();
        }
        
        return pizza;
    }
}
