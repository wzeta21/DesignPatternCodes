
/**
 * Write a description of class NYClamPizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NYClamPizza extends Pizza
{
    public NYClamPizza() {
        name = "NY Style Clam Pizza";
        dough = "Thin Crust Dough";
        sauce = "Clam Sauce";
        toppings.add("Grated Clam");
    }
}
