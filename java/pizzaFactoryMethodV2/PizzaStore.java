
/**
 * Client of The factory
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public abstract class PizzaStore
{
    
    // PizzaFactory factory;
    // public PizzaStore(PizzaFactory factory)
    // {
        // this.factory = factory;
    // }
    
    abstract Pizza createPizza(String type);
    
    public Pizza orderPizza(String type) {
        
        //IPizza pizza = factory.factoryMethod(type);
        Pizza pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
    
}
