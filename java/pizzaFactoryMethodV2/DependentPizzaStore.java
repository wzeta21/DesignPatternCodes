
/**
 * Write a description of class DependentPizzaStore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DependentPizzaStore 
{
    
    public Pizza createPizza(String style, String type){
        Pizza pizza = null;
        if (style.equals("NY")) {
            if (type.equals("cheese")) {
                pizza = new NYCheesePizza();
            } else if (type.equals("veggie")) {
                pizza = new NYVeggiePizza();
            } else if (type.equals("clam")) {
                pizza = new NYClamPizza();
            } else if (type.equals("pepperoni")) {
                pizza = new NYPepperoniPizza();
            }
        } else if (style.equals("Chicago")) {
            if (type.equals("cheese")) {
                pizza = new ChicagoCheesePizza();
            } else if (type.equals("veggie")) {
                pizza = new ChicagoVeggiePizza();
            } else if (type.equals("clam")) {
                pizza = new ChicagoClamPizza();
            } else if (type.equals("pepperoni")) {
                pizza = new ChicagoPepperoniPizza();
            }
        } else {
            System.out.println("Error: invalid type of pizza");
            return null;
        }
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
