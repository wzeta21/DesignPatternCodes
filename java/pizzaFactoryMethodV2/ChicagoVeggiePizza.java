
/**
 * Write a description of class ChicagoVeggiePizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ChicagoVeggiePizza extends Pizza
{
    public ChicagoVeggiePizza() {
        name = "Chicago Veggie Pizza";
        dough = "Extra Thick Crust Dough";
        sauce = "Veggie Sauce";
        toppings.add("Veggie");
    }
    @Override
    public void cut() {
        System.out.println("Cutting the Veggie pizza");
    }
}
