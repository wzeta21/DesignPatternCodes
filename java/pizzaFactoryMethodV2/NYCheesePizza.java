
/**
 * Write a description of class NYCheesePizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NYCheesePizza extends Pizza
{
    public NYCheesePizza() {
        name = "NY Style Sauce and Cheese Pizza";
        dough = "Thin Crust Dough";
        sauce = "Marinara Sauce";
        toppings.add("Grated Reggiano Cheese");
    }

}
