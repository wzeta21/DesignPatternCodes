
/**
 * Write a description of class IPIizzaFactory here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public abstract class PizzaFactory
{
    //public abstract Pizza factoryMethod(String type);
    // factory method
    abstract Pizza createPizza(String type);
}
