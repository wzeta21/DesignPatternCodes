
/**
 * Write a description of class NYStylePizzaStore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NYPizzaStore extends PizzaStore
{
    @Override
    public Pizza createPizza(String type)
    {
        Pizza pizza = null; 
        if (type.equals("cheese")) {
            pizza = new NYCheesePizza();
        } else if (type.equals("pepperoni")){
            pizza = new NYPepperoniPizza();
        } else if (type.equals("clam")){
            pizza = new NYClamPizza();
        } else if (type.equals("veggie")){
            pizza = new NYVeggiePizza();
        }
        
        return pizza;
    }
}
