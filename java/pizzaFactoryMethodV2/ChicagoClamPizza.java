
/**
 * Write a description of class ChicagoClamPizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ChicagoClamPizza extends Pizza
{
    public ChicagoClamPizza() {
        name = "Chicago Style Clam Pizza";
        dough = "Extra Thick Crust Dough";
        sauce = "clam Sauce";
        toppings.add("clam Cheese");
    }
    @Override
    public void cut() {
        System.out.println("Cutting the clam pizza");
    }
}
