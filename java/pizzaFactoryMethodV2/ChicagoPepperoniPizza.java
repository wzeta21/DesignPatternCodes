
/**
 * Write a description of class ChicagoPepperoniPizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ChicagoPepperoniPizza extends Pizza
{
    public ChicagoPepperoniPizza() {
        name = "Chicago Style Pepperoni Pizza";
        dough = "Extra Thick Crust Dough";
        sauce = "Pepperoni Sauce";
        toppings.add("Pepperoni Cheese");
    }
    @Override
    public void cut() {
        System.out.println("Cutting Pepperoni pizza");
    }
}
