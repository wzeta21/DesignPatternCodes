
/**
 * Class to Adapter.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class AcousticGuitar
{
    public void play(){
        System.out.println("Playing Guitar");
    }

    public void leaveGuitar(){
        System.out.println("I'm tired to play the guitar");
    }
}