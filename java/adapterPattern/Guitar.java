
/**
 * Abstract class Guitar - write a description of the class here
 *
 * @author (your name here)
 * @version (version number or date here)
 */
public abstract class Guitar
{
    abstract public void onGuitar();
    abstract public void offGuitar();
}
