
/**
 * Write a description of class ElectricGuitar here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ElectricGuitar extends Guitar
{
    public void onGuitar() {
        System.out.println("Playing Guitar");
    }

    public void offGuitar() {
        System.out.println("I'm tired to play the guitar");
    }
}
