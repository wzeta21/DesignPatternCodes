
/**
 * we Adapter AcousticGuitar into
 * ElectricAcousticGuitar to adapt into the GuitarModel
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ElectricAcousticGuitar extends Guitar
{
    AcousticGuitar acoustic = new AcousticGuitar();

    public void onGuitar() {
        acoustic.play();
    }

    public void offGuitar() {
        acoustic.leaveGuitar();
    }
}
