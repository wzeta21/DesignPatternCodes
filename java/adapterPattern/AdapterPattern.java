
/**
 * Write a description of class AdapterPattern here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class AdapterPattern
{
    public static void main(String[] arg) {
        Guitar eGuitar = new ElectricGuitar();
        eGuitar.onGuitar();
        eGuitar.offGuitar();
        
        Guitar eAGuitar = new ElectricAcousticGuitar();
        eAGuitar.onGuitar();
        eAGuitar.offGuitar();
    }
}
