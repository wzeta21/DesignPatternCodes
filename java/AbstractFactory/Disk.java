/**
 * Abstract class Disco - write a description of the class here
 * 
 * @author: 
 * Date: 
 */
public abstract class Disk implements Prototype
{
    //implentacion de clone() de la interface Prototype
    @Override
    public abstract Prototype clone();
 
    public abstract String getCapacity();
 
    public abstract String getName();
 
    public abstract String getPrice();
 
    @Override
    public String toString() {
            return getName() + " (" + getCapacity() + ")";
    }
}
