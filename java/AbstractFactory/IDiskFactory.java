
/**
 * Write a description of interface IDiskFactory here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public interface IDiskFactory
{
    //definicion de metodos de la interface
    public BluRay createBluRay();
    public DVD createDVD();
}
