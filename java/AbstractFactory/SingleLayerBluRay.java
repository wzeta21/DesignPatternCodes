
/**
 * Write a description of class BluRay_CapaSimple here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SingleLayerBluRay extends BluRay
{
    @Override
    public Prototype clone() {
            return new SingleLayerBluRay();
    }
 
    @Override
    public String getCapacity() {
            return "25GB";
    }
 
    @Override
    public String getName() {
            return "BluRay Capa Simple";
    }
 
    @Override
    public String getPrice() {
            return "20.00BS";
    }
}
