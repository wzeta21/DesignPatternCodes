/**
 * Write a description of class Sony here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SonyMain
{
    public static void main(String[] arg){
        
        IDiskFactory fabrica = new SingleLayerDiskFactory();
        
        DVD dvd = fabrica.createDVD();
        BluRay bluray = fabrica.createBluRay();

        System.out.println(dvd);
        System.out.println(bluray);

        fabrica = new DoubleLayerDiskFactory();
        dvd = fabrica.createDVD();
        bluray = fabrica.createBluRay();

        System.out.println(dvd);
        System.out.println(bluray);
    }
}
