
/**
 * Write a description of class FabricaDiscos_CapaSimple here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SingleLayerDiskFactory implements IDiskFactory
{
    // implementación de los metodos de la interface IDiskFactory
    @Override
    public BluRay createBluRay(){
        return new SingleLayerBluRay();
    }
    @Override
    public DVD createDVD(){
        return new SingleLayerDVD();
    }
}
