
/**
 * Write a description of class FabricaDiscos_CapaDoble here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DoubleLayerDiskFactory implements IDiskFactory
{
    // implementación de los metodos de la interface IDiskFactory
    @Override
    public BluRay createBluRay(){
        return new DoubleLayerBluRay();
    }
    @Override
    public DVD createDVD(){
        return new DoubleLayerDVD();
    }
}
