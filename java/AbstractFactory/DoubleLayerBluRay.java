
/**
 * Write a description of class BluRay_CapaDoble here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DoubleLayerBluRay extends BluRay
{
    @Override
    public Prototype clone() {
            return new DoubleLayerBluRay();
    }
 
    @Override
    public String getCapacity() {
            return "50GB";
    }
 
    @Override
    public String getName() {
            return "BluRay Capa Doble";
    }
 
    @Override
    public String getPrice() {
            return "30.00Bs";
    }
}
