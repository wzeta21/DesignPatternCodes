
/**
 * Write a description of class DVD_CapaSimple here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SingleLayerDVD extends DVD
{
    @Override
    public Prototype clone() {
        return new SingleLayerDVD();
    }

    @Override
    public String getCapacity() {
        return "4.7GB";
    }

    @Override
    public String getName() {
        return "DVD Capa Simple";
    }

    @Override
    public String getPrice() {
        return "5.00$";
    }
}
