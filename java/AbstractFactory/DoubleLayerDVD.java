
/**
 * Write a description of class DVD_CapaDoble here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DoubleLayerDVD extends DVD
{

    @Override
    public Prototype clone() {
        return new DoubleLayerDVD();
    }

    @Override
    public String getCapacity() {
        return "7GB";
    }

    @Override
    public String getName() {
        return "DVD Capa doble";
    }

    @Override
    public String getPrice() {
        return "8.00Bs";
    }
}
