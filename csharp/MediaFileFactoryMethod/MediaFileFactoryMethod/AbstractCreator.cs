﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaFileFactoryMethod
{
    abstract class AbstractCreator
    {
        /// <summary>
        /// abstract fatoryMethod
        /// </summary>
        /// <param name="filetype"></param>
        /// <returns></returns>
        public abstract IFile Create(FileType filetype);

    }
}
