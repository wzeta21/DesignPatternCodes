﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaFileFactoryMethod
{
    class Creator : AbstractCreator
    {
        public override IFile Create(FileType filetype)
        {
            IFile objeto = null;
            switch (filetype)
            {
                case FileType.AUDIO:
                    objeto = new AudioFile();
                    break;
                case FileType.VIDEO:
                    objeto = new VideoFile();
                    break;
            }
            return objeto;
        }
    }
}
