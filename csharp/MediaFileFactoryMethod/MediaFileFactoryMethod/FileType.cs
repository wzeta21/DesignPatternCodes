﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaFileFactoryMethod
{
    public enum FileType
    {
        AUDIO = 1,
        VIDEO = 2
    }
}
