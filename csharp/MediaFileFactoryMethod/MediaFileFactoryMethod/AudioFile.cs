﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaFileFactoryMethod
{
    class AudioFile : IFile
    {
        public void Play()
        {
            Console.WriteLine("Reproduciendo archivo de audio...");
        }
    }
}
