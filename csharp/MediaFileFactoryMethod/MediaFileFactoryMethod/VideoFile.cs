﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaFileFactoryMethod
{
    class VideoFile : IFile
    {
        public void Play()
        {
            Console.WriteLine("Reproduciendo archivo de vídeo...");
        }
    }
}
