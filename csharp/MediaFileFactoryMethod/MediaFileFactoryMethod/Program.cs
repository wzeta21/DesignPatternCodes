﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaFileFactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractCreator creator = new Creator();

            IFile audio = creator.Create(FileType.AUDIO);
            audio.Play();

            IFile video = creator.Create(FileType.VIDEO);
            video.Play();
            Console.ReadKey();
        }
    }
}
