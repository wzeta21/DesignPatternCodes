﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pizzaMethodFactory
{
    public abstract class PizzaFactory
    {
        /// <summary>
        /// Abastract method factory
        /// </summary>
        /// <param name="type">Type of pizza</param>
        /// <returns>Pizza</returns>
        public abstract IPizza FactoryMethod(string type);
    }
}
