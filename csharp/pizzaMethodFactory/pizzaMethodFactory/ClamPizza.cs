﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pizzaMethodFactory
{
    public class ClamPizza : IPizza
    {
        public ClamPizza()
        {
            Console.WriteLine("I am clam pizza!");
        }
        public void Bake()
        {
            
        }

        public void Box()
        {
            
        }

        public void Cut()
        {
            
        }

        public void Prepare()
        {
            
        }
    }
}
