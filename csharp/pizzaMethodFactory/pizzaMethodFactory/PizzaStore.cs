﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pizzaMethodFactory
{
    class PizzaStore
    {
        PizzaFactory factory;
        public PizzaStore(PizzaFactory factory)
        {
            this.factory = factory;
        }
        public IPizza OrderPizza(string type)
        {

            IPizza pizza = factory.FactoryMethod(type);
            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();
            return pizza;
        }
    }
}
