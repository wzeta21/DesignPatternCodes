﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pizzaMethodFactory
{
    public class CheesePizza : IPizza
    {
        public CheesePizza()
        {
            Console.WriteLine("I am cheese pizza!");
        }
        public void Bake()
        {
            
        }

        public void Box()
        {
            
        }

        public void Cut()
        {
            
        }

        public void Prepare()
        {
            Console.WriteLine("preparing cheez pizza");
        }
    }
}
