﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pizzaMethodFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            SimplePizzaFactory pizzaFactoy = new SimplePizzaFactory();
            PizzaStore pizzaStore = new PizzaStore(pizzaFactoy);

            IPizza cheesePizza = pizzaStore.OrderPizza("cheese");            

            IPizza clamPizza = pizzaStore.OrderPizza("clam");
            Console.ReadKey();
        }
    }
}
