﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pizzaMethodFactory
{
    public class SimplePizzaFactory : PizzaFactory
    {
        /// <summary>
        /// factory method implemented
        /// </summary>
        /// <param name="type">type of pizza</param>
        /// <returns>pizza</returns>
        public override IPizza FactoryMethod(string type)
        {
            IPizza pizza = null;
            if (type.Equals("cheese"))
            {
                pizza = new CheesePizza();
            }
            else if (type.Equals("pepperoni"))
            {
                pizza = new PepperoniPizza();
            }
            else if (type.Equals("clam"))
            {
                pizza = new ClamPizza();
            }
            else if (type.Equals("veggie"))
            {
                pizza = new VeggiePizza();
            }
            return pizza;
        }
    }
}
