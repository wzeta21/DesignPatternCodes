﻿using System;

namespace yield
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            new MyYieldClass().yieldMethodConsumer();
            Console.WriteLine("Other");
            new MyYieldClass().Consumer2(10, 77, 3);
        }
    }
}
