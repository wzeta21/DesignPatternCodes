using System;
using System.Collections.Generic;

namespace yield
{
    public class MyYieldClass
    {
        /// <summary>
        /// Simple exemplo to iterate
        /// </summary>
        /// <returns></returns>
        public IEnumerable<int> MyIntegers() {
            yield return 1;
            yield return 3;
            yield return 8;
            yield return 16;
            yield return 1000;
        }

        public void yieldMethodConsumer() {
            foreach (int item in MyIntegers())
            {
                Console.WriteLine("yield value {0} ", item);
            }
        }

        /// <summary>
        /// Multiples of NUM in a numbers range
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public IEnumerable<int> YO(int begin, int end, int num){
            for (int i = begin; i < end; i++)
            {
                if(i % num == 0){
                    //Console.WriteLine("yield value {i}", i);
                   yield return  i;
                }
            }
        }

        public void Consumer2(int begin, int end, int num){            
            foreach (int item in this.YO(begin, end, num))
            {
                Console.WriteLine("yield values: {0} ", item);
            }
        }
    }
    
}